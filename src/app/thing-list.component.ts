import {Component} from '@angular/core';
import {Sample} from './sdk/sample/Sample';
import {Inject} from "@angular/core";

@Component({
	moduleId: module.id,
	selector: 'thing-list',
	template: `<ul>
	<li *ngFor="let thing of things">{{thing.name}}</li>
</ul>`,
})
export class ThingList {
	things;

	constructor(@Inject("Sample.Api") private sample:Sample.Api) {
		this.sample.get().subscribe(
			things => {
				this.things = things;
			}
		);
	}
}
