import {Component} from '@angular/core';
import {Sample} from './sdk/sample/Sample';
import {Inject} from "@angular/core";

@Component({
	moduleId: module.id,
	selector: 'thing-adder',
	template: `
<input #thingName type="text" >
<input type="button" value="Add Thing" (click)="addThing(thingName.value)">
<span *ngIf="status === ADD_STATUS.NAME_TOO_LONG" style="color:red;">Name Too Long!</span>
<span *ngIf="status === ADD_STATUS.ALREADY_EXISTS" style="color:red;">Already Exists!</span>
`,
})
export class ThingAdder {
	ADD_STATUS = Sample.AddStatus;
	status = Sample.AddStatus.OK;

	constructor(@Inject("Sample.Api") private sample:Sample.Api) {
	}

	addThing(name:string) {
		this.sample.add(name).subscribe(status => {
			this.status = status;
		});
	}
}
