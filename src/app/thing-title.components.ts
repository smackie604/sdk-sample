import {Component} from '@angular/core';
import {Sample} from './sdk/sample/Sample';
import {Inject} from "@angular/core";

@Component({
	moduleId: module.id,
	selector: 'thing-title',
	template: `<h1>Things ({{count}})</h1>`,
})
export class ThingTitle {
	count;

	constructor(@Inject("Sample.Api") private sample:Sample.Api) {
		this.sample.get().subscribe(things => {
			this.count = things.length;
		});
	}
}
