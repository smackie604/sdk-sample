import {Observable} from "rxjs/Observable";
import {Sample} from "../Sample";

export interface DataPipe {
	get():Observable<Sample.Thing[]>;
	add(thing:Sample.Thing);
}
