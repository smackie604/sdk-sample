import {bootstrap} from '@angular/platform-browser-dynamic';
import {enableProdMode} from '@angular/core';
import {SampleFeatureAppComponent, environment} from './app';
import {SAMPLE_WIRING} from './app/sdk/sample/service/SampleWiring';


if (environment.production) {
	enableProdMode();
}

const LEAN_INJECTORS = [];
LEAN_INJECTORS.push(SAMPLE_WIRING);
bootstrap(SampleFeatureAppComponent, LEAN_INJECTORS);
