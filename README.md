# Install and Running#
```
#!bash
npm install -g angular-cli
cd sdk-sample
npm install
ng serve

```

The SDK part of the code is located at:

sdk-sample/src/app/sdk/sample

The running app just shows a few different components interacting with the SDK