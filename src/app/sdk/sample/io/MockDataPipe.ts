import {Sample} from "../Sample";
import {DataPipe} from "./DataPipe";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Injectable} from "@angular/core";

@Injectable()
export class MockDataPipe implements DataPipe {
	private state:BehaviorSubject<Sample.Thing[]>;

	constructor() {
		let json = `
		[{
			"id": 1,
			"name": "Thing1",
			"date": "2011-01-01T01:01:01.511Z"
		},
		{
			"id": 2,
			"name": "Thing2",
			"date": "2012-02-02T02:02:02.511Z"
		},
		{
			"id": 3,
			"name": "Thing3",
			"date": "2013-03-03T03:02:03.511Z"
		}]
		`;

		this.state = new BehaviorSubject(JSON.parse(json));
	}

	get():Observable<Sample.Thing[]> {
		return this.state.asObservable();
	}

	add(thing:Sample.Thing) {
		const things = this.state.getValue();

		thing.id = things.length + 1;

		return Observable.create(observer => {
			if (thing.name.length > 10) {
				observer.next(Sample.AddStatus.NAME_TOO_LONG);
			}
			else if (things.find(t => t.name == thing.name)) {
				observer.next(Sample.AddStatus.ALREADY_EXISTS);
			}
			else {
				things.push(thing);
				this.state.next(things);
				observer.next(Sample.AddStatus.OK);
			}
			observer.complete();
		});
	}
}
