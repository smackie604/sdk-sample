import {Component} from '@angular/core';
import {ThingList} from "./thing-list.component";
import {ThingTitle} from "./thing-title.components";
import {ThingAdder} from "./thing-adder.component";

@Component({
	moduleId: module.id,
	selector: 'sample-feature-app',
	template: `
<thing-adder></thing-adder>
<thing-title></thing-title>
<thing-list></thing-list>
`,
	directives: [ThingList, ThingTitle, ThingAdder]
})
export class SampleFeatureAppComponent {
}
