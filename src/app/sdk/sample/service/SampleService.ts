import {Sample} from "../Sample";
import {Injectable, Inject} from "@angular/core";
import {MockDataPipe} from "../io/MockDataPipe";

@Injectable()
export class SampleService implements Sample.Api {
	constructor(@Inject("Sample.DataPipe") private dataPipe:MockDataPipe) {
	}

	get() {
		return this.dataPipe.get();
	}

	add(thingName:string) {
		let thing = new Sample.Thing();
		thing.name = thingName;
		thing.date = new Date();
		return this.dataPipe.add(thing);
	}
}
