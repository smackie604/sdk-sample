import { SampleFeaturePage } from './app.po';

describe('sample-feature App', function() {
  let page: SampleFeaturePage;

  beforeEach(() => {
    page = new SampleFeaturePage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('sample-feature works!');
  });
});
