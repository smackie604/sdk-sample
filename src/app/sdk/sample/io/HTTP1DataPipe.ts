import {Http} from "@angular/http";
import {Sample} from "../Sample";
import {DataPipe} from "./DataPipe";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Injectable} from "@angular/core";

@Injectable()
class HTTP1DataPipe implements DataPipe {
	constructor(private http:Http) {
	}

	private state:BehaviorSubject<Sample.Thing[]> = new BehaviorSubject(null);

	private refresh() {
		if (!this.state) {
			this.http.get('/sample').subscribe(things => {
				this.state.next(things.json() as Sample.Thing[]);
			});
		}
	}

	get():Observable<Sample.Thing[]> {
		if (!this.state.getValue()) {
			this.refresh()
		}
		return this.state.asObservable();
	}

	add(thing:Sample.Thing) {
		return Observable.create(observer => {
			this.http.post('/sample', JSON.stringify(thing))
				.subscribe(res => {
					switch (res.status) {
						case(204):
							this.refresh();
							observer.onNext(Sample.AddStatus.OK);
							break;
						case(400):
							observer.onNext(Sample.AddStatus.NAME_TOO_LONG);
							break;
						case(409):
							observer.onNext(Sample.AddStatus.ALREADY_EXISTS);
							break;
						default:
							console.log("Unknown error code: " + res.status);
							observer.onError(Sample.AddStatus.UNKNOWN)
					}
					observer.onComplete();
				});
		});
	}
}
