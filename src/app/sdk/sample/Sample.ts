import {Observable} from "rxjs/Observable";

export module Sample {

	export interface Api {
		get():Observable<Thing[]>;
		add(thing:string):Observable<AddStatus>
	}

	export class Thing {
		id:number;
		name:string;
		date:Date;
	}

	export enum AddStatus{
		OK,
		NAME_TOO_LONG,
		ALREADY_EXISTS,
		UNKNOWN
	}
}
