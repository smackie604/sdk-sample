export class SampleFeaturePage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('sample-feature-app h1')).getText();
  }
}
