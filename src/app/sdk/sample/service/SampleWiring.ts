import {SampleService} from "./SampleService";
import {MockDataPipe} from "../io/MockDataPipe";
import {provide} from '@angular/core';

export const SAMPLE_WIRING = [
	provide("Sample.DataPipe", {useClass: MockDataPipe}),
	provide("Sample.Api", {useClass: SampleService})
];
